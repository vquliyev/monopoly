package com.vagif.monopoly;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class MonopolyApplication {

	@GetMapping("/")
	String home() {
		return "Monopoly Landing Page!";
	}

	// http://localhost:8080

	public static void main(String[] args) {
		SpringApplication.run(MonopolyApplication.class, args);
	}
}