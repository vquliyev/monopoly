package com.vagif.monopoly.controller;

import com.vagif.monopoly.model.Citizen;
import com.vagif.monopoly.model.City;
import com.vagif.monopoly.service.CitizenService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/citizens")
@Data
public class CitizenController {

    private final CitizenService citizenService;

    public CitizenController(@Autowired CitizenService citizenService) {
        this.citizenService = citizenService;
    }

    @GetMapping
    public String show_something() {
        return "Citizens controller working...";
    }

    @GetMapping("/{id}}")
    public Optional<Citizen> get_one(@PathVariable("id") Long id) {
        return citizenService.get_one(id);
    }

    @GetMapping("/city/id")
    public City get_one_city(@PathVariable("id") Long id) {
        return citizenService.get_city(id);
    }

}
