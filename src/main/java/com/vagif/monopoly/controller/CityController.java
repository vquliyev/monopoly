package com.vagif.monopoly.controller;

import com.vagif.monopoly.model.Citizen;
import com.vagif.monopoly.service.CityService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Data
@RequestMapping("/cities")
public class CityController {

    private final CityService cityService;

    public CityController(@Autowired CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping
    public String show_something() {
        return "Cities controller working...";
    }

    @GetMapping("/{id}")
    public Iterable<Citizen> get_all_citizens(@PathVariable("id") Long id) {
        return cityService.get_all_citizens(id);
    }

}
