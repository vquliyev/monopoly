package com.vagif.monopoly.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Citizen {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "citizen_id")
    private Long id;

    @NotNull
    @Size(min = 1, message = "This filed can't be empty")
    private String name;

    private String surname;

    private GENDER gender;

    private String additionalInfo;

    @ManyToOne
    @JoinTable(name = "r_citizen_city",
            joinColumns = @JoinColumn(name = "citizen_id", referencedColumnName = "citizen_id"),
            inverseJoinColumns = @JoinColumn(name = "city_id")
    )
    private City city;

}
