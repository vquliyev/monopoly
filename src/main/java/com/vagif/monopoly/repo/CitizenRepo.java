package com.vagif.monopoly.repo;

import com.vagif.monopoly.model.Citizen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CitizenRepo extends JpaRepository<Citizen, Long> {
}
