package com.vagif.monopoly.repo;

import com.vagif.monopoly.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepo extends JpaRepository<City, Long> {
}
