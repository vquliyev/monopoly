package com.vagif.monopoly.service;

import com.vagif.monopoly.model.Citizen;
import com.vagif.monopoly.model.City;
import com.vagif.monopoly.repo.CitizenRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Data
public class CitizenService {

    private final CitizenRepo citizenRepo;


    public CitizenService(@Autowired CitizenRepo citizenRepo) {
        this.citizenRepo = citizenRepo;
    }

    public Optional<Citizen> get_one(Long id) {
        return citizenRepo.findById(id);
    }

    public City get_city(Long id) {
        return citizenRepo.findById(id).get().getCity();
    }


}
