package com.vagif.monopoly.service;


import com.vagif.monopoly.model.Citizen;
import com.vagif.monopoly.repo.CityRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
public class CityService {

    private final CityRepo cityRepo;

    public CityService(@Autowired CityRepo cityRepo) {
        this.cityRepo = cityRepo;
    }

    public List<Citizen> get_all_citizens(Long id) {
        return cityRepo.findById(id).get().getCitizens();
    }

}
